from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes 
from rest_framework.permissions import IsAuthenticated 
from django.http import JsonResponse 
from django.views.decorators.csrf import csrf_exempt  
from .serializers import CarSerializer,ImageSerializers
from .models import Car,imagesoffer
from rest_framework import status 
import json 
from django.core.exceptions import ObjectDoesNotExist   
from .helpers import modify_input_for_multiple_files

from rest_framework.response import Response

#Get all the cars   
@api_view(["GET"]) 
def get_cars(request):
    user= request.user.id 
    cars= Car.objects.all()
    serializer=CarSerializer(cars,many=True)
    return JsonResponse({'cars':serializer.data})  

#Get car by name     
@api_view(["GET"])
def Get_Car_Byname(request,pk):
    car = Car.objects.filter(Title=pk)
    serializer = CarSerializer(car,many=True)
    return Response(serializer.data)

#Add new car
@api_view(["POST"]) 
def add_car(request):    
    offer_id = request.data['offer_id'] 
    images = dict((request.data).lists())['Photo'] 
    flag = 1 
    arr = [] 
    for image in images: 
        modified_data = modify_input_for_multiple_files(offer_id,image)
        file_serilazers = ImageSerializers(data=modified_data)
        if file_serilazers.is_valid():
            file_serilazers.save()
            arr.append(file_serilazers.data)
        else:
            flag = 0
    if flag == 1: 
        return Response(arr,status=status.HTTP_201_CREATED)
    else:
        return Response(arr,status=status.HTTP_400_BAD_REQUEST) 
  
 
#Get image 
@api_view(["GET"])   
def get_image(request,pk):
    user= request.user.id 
    image = imagesoffer.objects.filter.URL(Photo1=pk)
    serializer = ImageSerializers(image,many=True)
    return Response(serializer.data) 
   