from django.db import models  
from django.conf import settings 


class seller(models.Model):
    Name = models.CharField(max_length=50) 
    Email = models.CharField(max_length=50) 
    Phone = models.CharField(max_length=50)   

    def __str__(self):
        return self.Name
#create Car model here..
class Car(models.Model): 
    seller_id = models.ForeignKey(seller,on_delete=models.CASCADE) 
    Title = models.CharField(max_length=200) 
    Description = models.CharField(max_length=200)  
    Make = models.CharField(max_length=100) 
    Model = models.CharField(max_length=100)
    Price = models.CharField(max_length=200) 
    album_id = models.CharField(max_length=200) 
    Type_of_vehicle = models.CharField(max_length=100) 
    Vehicle_Number = models.CharField(max_length=50) 
    Mileage =models.CharField(max_length=100) 
    Power = models.CharField(max_length=100) 
    Fuel = models.CharField(max_length=100) 
    Fuel_Consumption = models.CharField(max_length=100)  
    Number_of_doors = models.CharField(max_length=10) 
    Gearbox = models.CharField(max_length=100) 
    Year_model = models.CharField(max_length=20)
    Color = models.CharField(max_length=20)
    Color_Interior = models.CharField(max_length=100) 
    Maximum_speed = models.CharField(max_length=100) 
    Number_of_seats = models.CharField(max_length=20)
   
    def __str__(self):
        return self.Title

class imagesoffer(models.Model):
    offer_id = models.ForeignKey(Car,on_delete=models.CASCADE,related_name='cars')  
    Photo = models.ImageField(upload_to='images',blank=True,null=True) 
    def __str__(self):
        return '/media/'+ self.Photo.name
  
