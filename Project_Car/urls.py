"""Project_Car URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin 
from django.urls import include,path 
from django.conf import settings 
from django.conf.urls.static import static  
from . import views
from .api import Make 
from .api import Model 
from .api import Cars  

urlpatterns = [
    path('admin/', admin.site.urls), 
    
    path('make/',Make.as_view()), 
    path('model/',Model.as_view()), 
    path('car/',Cars.as_view()),
    path('getcars/' , views.get_cars),
    
    path('Search_Car/<str:pk>/', views.Get_Car_Byname),
    path('Add_Car/', views.add_car),
    path('media/images/<str:pk>/', views.get_image)

]
if settings.DEBUG:
    urlpatterns +=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)