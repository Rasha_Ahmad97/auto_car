from rest_framework import serializers
from .models import Car,imagesoffer

class ImageSerializers(serializers.ModelSerializer):
   class Meta:
        model = imagesoffer
        fields = ('offer_id','Photo')  
        
class CarSerializer(serializers.ModelSerializer): 
    cars = serializers.StringRelatedField(many=True)
    class Meta:
        model = Car
        fields =('seller_id','Title','Description','Make','Model','Price','album_id','Type_of_vehicle'
        ,'Vehicle_Number','Mileage','Power','Fuel','Fuel_Consumption','Number_of_doors','Gearbox','Year_model'
        ,'Color','Color_Interior','Maximum_speed','Number_of_seats','cars')
