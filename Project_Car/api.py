from rest_framework import generics
from rest_framework import response 
from django.http import JsonResponse  

import pandas as pd  
import json 
df = pd.read_csv('C://Users//Rasha//Downloads//Car_Model_ListCsv.csv') 
make = df.Make.unique()  
make_list = make.tolist()
model = df.Model.unique() 
model_list = model.tolist() 
df_new2 = df.iloc[:,2:4] 
Make_Model=df_new2.groupby(["Make"],as_index=True).apply(lambda x: x.to_json(orient='index')) 

class Make(generics.ListCreateAPIView):
    def get(self,request): 
        return JsonResponse(json.dumps(make_list), safe=False)
class Model(generics.ListCreateAPIView):
    def get(self,request):
         return JsonResponse(json.dumps(model_list), safe=False) 

class CategoryByMake(generics.ListCreateAPIView):
    def get(self,request):
        return JsonResponse(json.dumps(Make_Model), safe=False)  

 

class Cars(generics.ListCreateAPIView):
    def get(self,request): 
        data = [ { "Title":"Mercedes-Benz C 220 d BlueTEC", "Description":"Schiebed/Leder/Navi/Led/Sportsitz", "Price":"94,154 AED", "album_id":1, "Type of vehicle":"Saloon", "Vehicle_Number": "BenzPrlr", "Mileage":"96,500 Km (used)", "Power":"170 Ch", "Fuel": "Diesel", "Fuel_Consumption" : "4,0 l/100km", "Number of doors":"4", "Gearbox":"Manual", "Year_model":"2017", "Color":"Grey", "Color_Interior":"Cuir partiel, Blanc", "Maximum speed":"234 km/h", "Number of Seats":5 }, { "Title":"Mercedes-Benz C 220 Elegance CDI", "Description":"Elegance CDI", "Price":"7364 AED", "album_id":2, "Type of vehicle":"Saloon", "Vehicle_Number":"BenzPrlryd", "Mileage":"190,000 Km", "Power":"143 Ch", "Fuel":"Diesel", "Fuel_Consumption":"6.3 l/100km", "Number of doors":4, "Gearbox":"Manual", "Year_model":"2003", "Color":"Silver", "Color_Interior":"Cuir partiel, Blanc", "Maximum speed":"200 km/h", "Number of Seats":5 }, { "Title":"Mercedes-Benz C 220 C 220 Cabrio", "Description":"VOLL,HYACINTHROT,Leder,LED,AHK,Erstb", "Price":"146812 AED", "album_id":3, "Type of vehicle":"Convertible", "Vehicle_Number":"Ghrsds", "Mileage":"98,100 Km", "Power":"170ch", "Fuel":"Diesel", "Fuel_Consumption":"4.5 l/100km", "Number of doors":2, "Gearbox":"Manual", "Year_model":"2016", "Color":"Red", "Color_Interior":"Cuir partiel, Noir", "Maximum speed":"231 km/h", "Number of Seats":5 }, { "Title":"Mercedes-Benz C 180 ", "Description":"/AMG/Widescreen/360camera/Standkachel/stoelverw/Le", "Price":"146617", "album_id":4, "Type of vehicle":"Saloon", "Vehicle_Number":"BenzPrlruy", "Mileage":"41,899 Km", "Power":"156 Ch", "Fuel":"Petrol", "Fuel_Consumption":"7 l/100km", "Number of doors":4, "Gearbox":"Automatic", "Year_model":"2019", "Color":"Black", "Color_Interior":"Cuir, Noir", "Maximum speed":"222 km/h", "Number of Seats":5 } ]
        return JsonResponse(data, safe=False) 

